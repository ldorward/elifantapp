//
//  LoginViewController.swift
//  Hive
//
//  Created by Sean MacPherson on 4/20/16.
//  Copyright © 2016 Lukas Dorward. All rights reserved.
//

import Foundation
import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var hiveLabel: UILabel!
    
    @IBOutlet weak var userField: UITextField!
    
    @IBOutlet weak var passField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor(red: 59/255, green: 190/255, blue: 152/255, alpha: 1.0)
        
        self.hiveLabel.textColor = UIColor.whiteColor()
        
    }

    

    
    override func viewDidLayoutSubviews() {
        
        //both border's specs
        
        let color = UIColor.whiteColor().CGColor
        let width = CGFloat(0.5)
        
        //userField bottom border
        
        let userBorder = CALayer()
        userBorder.borderColor = color
        userBorder.frame = CGRect(x: 0, y: userField.frame.size.height - width, width:  userField.frame.size.width, height: userField.frame.size.height)
        
        userBorder.borderWidth = width
        userField.layer.addSublayer(userBorder)
        userField.layer.masksToBounds = true
        
        
        //passField Bottom border
        
        let passBorder = CALayer()
        passBorder.borderColor = color
        passBorder.frame = CGRect(x: 0, y: passField.frame.size.height - width, width:  passField.frame.size.width, height: passField.frame.size.height)
        
        passBorder.borderWidth = width
        passField.layer.addSublayer(passBorder)
        passField.layer.masksToBounds = true

    }
    

    
}